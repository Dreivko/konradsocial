Rails.application.routes.draw do
  
  root 'pages#index'

  #==================
  # Resources
  #==================
  resources :relations
  resources :likes
  resources :entries
  resources :telephones
  resources :emails
  resources :users

  #==================
  # Api's
  #==================


  get '/users/api/json', to: 'users#api'
  get '/telephones/api/json', to: 'telephones#api'
  get '/relations/api/json', to: 'relations#api'
  get '/likes/api/json', to:'likes#api'
  get '/entries/api/json', to: 'entries#api'
  get '/emails/api/json', to: 'emails#api'

  post '/users/hiden', to:'users#hiden'
  post '/telephones/hiden', to:'telephones#hiden'
  post '/relations/hiden', to:'relations#hiden'
  post '/likes/hiden', to:'likes#hiden'
  post '/entries/hiden', to:'entries#hiden'
  post '/emails/hiden', to:'emails#hiden'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
